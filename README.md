- Clone the repo https://bitbucket.org/Ayyad/valeo-task/src/master/

- Compose up the docker file to build the containers using the below command:
	  sudo docker-compose up -d

- Run  below command to go inside image:
	  sudo docker exec -it drupal1 bash

- Run composer update and make sure drush console work normally.

- Visit the site using port 9999  http://localhost:9999/

- Start setup the drupal project  
	- mysql user : root
	- mysql password : 123
	(I kept settings.php file inside the repo to facilitate the installing process ) 
	(also, I exported the database you will find it in the base folder if you want to import it and run the project directly)

- After installing you just need to enable valeo_core module using the dashboard from Extend page or  the below command :
	  drush en valeo_core

- Run the below command to make sure that the features were imported or form features page from dashboard page :
	  drush fia -y 

- Run drush cr to clear the caches or from performance page from dashboard

- The task should be ready 
