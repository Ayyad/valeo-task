<?php

namespace Drupal\valeo_task\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;


class CreateTask extends Formbase{

  public function getFormId()
  {
    return 'valeo_task_creation_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $logged_in = \Drupal::currentUser()->isAuthenticated();
    if ($logged_in) {

      $form['#attributes'] = array(
        'class' => array('create-task-form container')
      );

      $form['task_fieldset'] = [
        '#type' => 'fieldset',
        '#title' => t('Task'),
        '#prefix' => '<div class="container">',
        '#suffix' => '</div>',
      ];

      $form['task_fieldset']['project_id'] = [
        '#type' => 'textfield',
        '#title' => 'Project ID',
        '#required' => true,
        '#weight' => 1,
        '#attributes' => [
          'placeholder' => 'Example: PRJ_ABCDEF',
          'class' => ['form-control'],
        ],
        '#prefix' => '<div class="form-group">',
        '#suffix' => '</div>',
      ];

      $form['task_fieldset']['task_type'] = [
        '#type' => 'radios',
        '#title' => 'Task Type',
        '#options' => [
          'words' => 'Count Words',
          'lines' => 'Count Lines',
          'characters' => 'Count Characters',
        ],
        '#default_value' => 'words',
        '#required' => TRUE,
        '#weight' => 2,
        '#prefix' => '<span class="form-group">',
        '#suffix' => '</span>',
      ];

      $form['task_fieldset']['task_file'] = [
        '#type' => 'managed_file',
        '#title' => $this->t('Input file'),
        '#required' => true,
        '#weight' => 3,
        '#upload_validators' => [
          'file_validate_extensions' => ['txt'],
          'file_validate_size' => [1 * 1024 * 1024],
        ],
        '#attributes' => [
          'class' => ['form-control-file']
        ],
        '#prefix' => '<div class="form-group">',
        '#suffix' => '</div>',
      ];

      $form['task_fieldset']['submit'] = [
        '#type' => 'submit',
        '#value' => 'Submit',
        '#weight' => 4,
        '#attributes' => [
          'class' => ['btn', 'btn-primary']
        ],
        '#prefix' => '<div class="form-group">',
        '#suffix' => '</div>',
      ];
    }else{
      return $this->redirect('user.login', ['destination' => "/create-task"]);
    }
    return $form;
  }


  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $form_values = $form_state->cleanValues()->getValues();

    //to validate if the id start with PRJ_
    if(strpos( $form_values['project_id'], "PRJ_" ) === false){
      $form_state->setErrorByName('project_id', t('The project id must start with the PRJ_ .'));
    }
    //to validate there is id after the prefix PRJ_
    if (strlen( $form_values['project_id'])  <= 4){
      $form_state->setErrorByName('project_id', t('The project id must have id after PRJ_.'));
    }
  }


  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $task_service = \Drupal::service('valeo_task.task_service');
    $form_values = $form_state->cleanValues()->getValues();
    $project_id  = $form_values['project_id'];
    $file = File::load($form_values['task_file'][0]);
    $file_uri = $file->getFileUri();
    $file_divided_by_lines =  file($file_uri);

      //create task id
      $task_id = bin2hex(random_bytes(10));
      //get task type
      $task_type  = $form_values['task_type'];
      //log project status
      $task_service->logProjectStatus($project_id);
      //log task status
      $task_service->logTaskStatus( $task_id , $project_id , $task_type);
      //make a custom patch to counting...
      $operations = [
        ['countFileContent', [$file_divided_by_lines , $task_type , $task_id , $project_id]],
      ];
      $batch = [
        'title' => $this->t('Counting...'),
        'operations' => $operations,
        'finished' => 'task_finished',
      ];
      batch_set($batch);

      $form_state->setRedirect('<front>');
  }
}
