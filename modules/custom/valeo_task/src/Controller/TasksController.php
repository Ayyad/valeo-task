<?php

namespace Drupal\valeo_task\Controller;

use Drupal\Core\Controller\ControllerBase;


class TasksController extends ControllerBase{

  /**
   * {@inheritdoc}
   */
  public function __construct()
  {
    $this->task_service = \Drupal::service('valeo_task.task_service');
  }

  public function getTasks($project_id)
  {
    $logged_in = \Drupal::currentUser()->isAuthenticated();
    if ($logged_in) {
      $tasks = $this->task_service->getProjectTasks($project_id);

      return [
        '#theme' => 'tasks_listing',
        '#cache' => ['max-age' => 0],
        '#tasks' => $tasks,
      ];

    } else {
      return $this->redirect('user.login', ['destination' => "/project/{$project_id}/tasks"]);
    }

  }

}
