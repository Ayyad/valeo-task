<?php

namespace Drupal\valeo_task\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ProjectListing' block.
 *
 * @Block(
 *  id = "project_listing",
 *  admin_label = @Translation("Project Listing"),
 * )
 */
class ProjectListing extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $task_service = \Drupal::service('valeo_task.task_service');

    $projects  = $task_service->getProjects();
    $build = [];
    $build['project_listing']['#markup'] = \Drupal::theme()->render('project_listing',
      array(
        'projects' => $projects,
      )
    );
    $build['#cache'] = ['max-age' => 0];

    return $build;
  }

}
