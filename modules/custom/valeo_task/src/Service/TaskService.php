<?php

namespace Drupal\valeo_task\Service;

use Drupal\Core\Database\Database;
use PDO;

 class TaskService{
  /**
   * log project if insert new project or update existed one
   * @param $project_id
   */
  public function logProjectStatus($project_id){

    $query = \Drupal::database()->merge('valeo_project');
    $query->key([
      'project_id' => $project_id,
    ]);
    $query->fields([
      'project_id' => $project_id,
      'project_status' => '1',
      'created' =>   time(),
    ]);
    try {
      $result = $query->execute();
    }
    catch (\Exception $exception) {
      \Drupal::logger('valeo_task')
        ->error("Error adding log to the  project id =  {$project_id}. Exception: @exc",
          ['@exc' => $exception->getMessage()]);
    }
  }

   /**
    * log task status
    * @param $task_id
    * @param $project_id
    * @param $task_type
    */
  public function logTaskStatus($task_id , $project_id , $task_type){
    $query = \Drupal::database()->insert('valeo_task');
    $query->fields([
      'task_id' => $task_id,
      'task_type' => $task_type,
      'task_status' => '2',
      'occurrences' => '0',
      'created' => time(),
      'start_date' => null,
      'end_date' => null,
      'project_id' => $project_id,
    ]);
    try {
      $result = $query->execute();
    }
    catch (\Exception $exception) {
      \Drupal::logger('valeo_task')
        ->error("Error adding log to the  task id =  {$task_id}. Exception: @exc",
          ['@exc' => $exception->getMessage()]);
    }
  }

   /**
    * log start time for the task
    * @param $task_id
    */
  public function addTaskStartTime($task_id ){
    $query = $this->taskUpdateQuery($task_id);
    $query->fields([
      'start_date' => time(),
    ]);
    try {
      $result = $query->execute();
    }
    catch (\Exception $exception) {
      \Drupal::logger('valeo_task')
        ->error("Error updating start time  to the  task id =  {$task_id}. Exception: @exc",
          ['@exc' => $exception->getMessage()]);
    }
  }

   /**
    * end Task in current time
    * @param $task_id
    * @param $occurrences
    */
  public function EndTask($task_id , $occurrences ){
    $query = $this->taskUpdateQuery($task_id);

    $task_status = ($occurrences == '0') ? '0' : '1';
    $query->fields([
      'task_status' => $task_status,
      'end_date' => time(),
    ]);
    try {
      $result = $query->execute();
    }
    catch (\Exception $exception) {
      \Drupal::logger('valeo_task')
        ->error("Error updating start time  to the  task id =  {$task_id}. Exception: @exc",
          ['@exc' => $exception->getMessage()]);
    }
  }

   /**
    * end project in current time
    * @param $project_id
    */
  public function EndProject($project_id){

    $query = \Drupal::database()->update('valeo_project');
    $query->condition('project_id' , $project_id);
    $query->fields([
      'project_status' => '0',
    ]);
    try {
      $result = $query->execute();
    }
    catch (\Exception $exception) {
      \Drupal::logger('valeo_task')
        ->error("Error updating start time  to the  task id =  {$project_id}. Exception: @exc",
          ['@exc' => $exception->getMessage()]);
    }
  }

   /**
    * log task occurrences line by line
    * @param $task_id
    * @param $occurrences
    */
  public function addTaskOccurrences($task_id , $occurrences ){
    $query = $this->taskUpdateQuery($task_id);
    $query->fields([
      'occurrences' => $occurrences,
    ]);
    try {
      $result = $query->execute();
    }
    catch (\Exception $exception) {
      \Drupal::logger('valeo_task')
        ->error("Error updating start time  to the  task id =  {$task_id}. Exception: @exc",
          ['@exc' => $exception->getMessage()]);
    }
  }

   /**
    * @param $task_id
    * @return \Drupal\Core\Database\Query\Update
    */
  public function taskUpdateQuery($task_id){
    $query = \Drupal::database()->update('valeo_task');
    $query->condition('task_id' , $task_id);
    return $query;
  }

   /**
    * get all projects
    * @return mixed
    */
  public function getProjects(){
    $projects = $this->getProjectsMainQuery();
    $projects_arr = $this->prepareProjectsFields($projects);
    return $projects_arr;
  }

   /**
    * get project tasks
    * @param $project_id
    * @return mixed
    */
  public function getProjectTasks($project_id  ){
      $query = \Drupal::database()->select('valeo_task', 'task');
      $query->fields('task');
      $query->condition('task.project_id' , $project_id);
      $query->orderBy('task.created'  , 'DESC');
      $result = $query->execute()->fetchAll();
      return $result;
  }

   /**
    * get all Tasks status to specific project
    * @param $project_id
    * @param $count
    * @return mixed
    */
  public function getProjectTasksStatus($project_id , $count ){
    $connection = \Drupal::database();
    $connection->query("SET SQL_MODE=''");
    $q = "SELECT task_id, task_type, project_id, occurrences, task_status FROM
 (
   SELECT task_id, task_type, project_id, occurrences, task_status,
   @task_rank := IF(@current_task = task_type, @task_rank + 1, 1 ) AS task_rank,
   @current_task := task_type
   FROM valeo_task
   WHERE  project_id = '{$project_id}'
   ORDER BY task_type, created DESC
 ) ranked
 WHERE task_rank <={$count};";

    $query = $connection->query($q);
    $result = $query->fetchAll();
    return $result;
  }

   /**
    * @return false
    */
  private function getProjectsMainQuery(){
    $query = \Drupal::database()->select('valeo_project', 'project');
    $query->addExpression('project.project_id' , 'project_id');
    $query->addExpression('project.project_status' , 'project_status');
    $result = $query->execute()->fetchAll();
    return isset($result)  ? $result : false;
  }

   /**
    * @param $projects
    * @return mixed
    */
  private function prepareProjectsFields($projects){
    foreach ($projects as $project){
      $tasks_arr = null;
      $count = '5';
      //get last 5 tasks status;
      $project_tasks = $this->getProjectTasksStatus($project->project_id , $count);
      foreach($project_tasks as $val) {
        $tasks_arr[$val->task_type][] = $val;
      }
      $project->project_tasks = $tasks_arr;
    }
    return $projects;
  }

}
