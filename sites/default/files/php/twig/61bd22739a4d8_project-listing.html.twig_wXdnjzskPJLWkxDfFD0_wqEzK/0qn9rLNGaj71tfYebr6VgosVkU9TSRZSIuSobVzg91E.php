<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/valeo_task/templates/project-listing.html.twig */
class __TwigTemplate_63225247bd40d60f0a42e6fcb8f86b3dbec5c4e85ecc3c04eae9ecfa3ce783f6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container\">
<div class=\"buttons-container\">
  <span>
  <div> <a class=\"btn btn-primary\" href=\"";
        // line 4
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("valeo_task.create_task_form"));
        echo "\" title=\"Create Task\">";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Create Task"));
        echo "</a></td> </div>
    </span>
  <span>
    ";
        // line 7
        if (($context["logged_in"] ?? null)) {
            // line 8
            echo "  <div> <a class=\"btn btn-danger\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("user.logout"));
            echo "\" title=\"logout\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Logout"));
            echo "</a></td> </div>
  ";
        } else {
            // line 10
            echo "  <div> <a class=\"btn btn-primary\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("user.login"));
            echo "\" title=\"Login\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Login"));
            echo "</a></td> </div>
    ";
        }
        // line 12
        echo "
  </span>
</div>

  ";
        // line 16
        if (twig_test_empty(($context["projects"] ?? null))) {
            // line 17
            echo "  <p class=\"empty-message\"> There is no data found</p>
  ";
        } else {
            // line 19
            echo "  <table class=\"table table-sm project-table \">
  <thead>
  <tr class=\"table-danger\">
    <th scope=\"col\">Project</th>
    <th scope=\"col\">Task</th>
    <th scope=\"col\">Running</th>
    <th scope=\"col\">Link</th>
  </tr>
  </thead>
  <tbody>
  ";
            // line 29
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["projects"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 30
                echo "    <tr class=\"\">
      <th scope=\"row\">";
                // line 31
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["p"], "project_id", [], "any", false, false, true, 31), 31, $this->source), "html", null, true);
                echo "</th>
      <td>

          ";
                // line 34
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["p"], "project_tasks", [], "any", false, false, true, 34));
                foreach ($context['_seq'] as $context["key"] => $context["task"]) {
                    // line 35
                    echo "          <div class=\"tasks\">
            <span> ";
                    // line 36
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["key"], 36, $this->source), "html", null, true);
                    echo "</span>

                <span class=\"margin-right-10\">
                  ";
                    // line 39
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["task"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
                        // line 40
                        echo "                  ";
                        if ((twig_get_attribute($this->env, $this->source, $context["t"], "task_status", [], "any", false, false, true, 40) == "0")) {
                            // line 41
                            echo "                    ";
                            $context["status_indicator"] = "fail";
                            // line 42
                            echo "                  ";
                        } elseif ((twig_get_attribute($this->env, $this->source, $context["t"], "task_status", [], "any", false, false, true, 42) == "1")) {
                            // line 43
                            echo "                    ";
                            $context["status_indicator"] = "success ";
                            // line 44
                            echo "                  ";
                        } elseif ((twig_get_attribute($this->env, $this->source, $context["t"], "task_status", [], "any", false, false, true, 44) == "2")) {
                            // line 45
                            echo "                    ";
                            $context["status_indicator"] = "loading";
                            // line 46
                            echo "                  ";
                        }
                        // line 47
                        echo "                  <span class=\"indicator-circle ";
                        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["status_indicator"] ?? null), 47, $this->source), "html", null, true);
                        echo "\">  </span>
                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 49
                    echo "                </span>

          </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['task'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 53
                echo "
      </td>
      ";
                // line 55
                if ((twig_get_attribute($this->env, $this->source, $context["p"], "project_status", [], "any", false, false, true, 55) == 1)) {
                    // line 56
                    echo "      <td> yes </td>
      ";
                } else {
                    // line 58
                    echo "        <td> no </td>
      ";
                }
                // line 60
                echo "        <td> <a class=\"btn btn-round-border\" href=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("valeo_task.list_tasks", ["project_id" => twig_get_attribute($this->env, $this->source, $context["p"], "project_id", [], "any", false, false, true, 60)]), "html", null, true);
                echo "\" title=\"Project page\">";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Project page"));
                echo "</a></td>
    </tr>
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 63
            echo "  ";
        }
        // line 64
        echo "  </tbody>
</table>
</div>
";
    }

    public function getTemplateName()
    {
        return "modules/custom/valeo_task/templates/project-listing.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 64,  189 => 63,  177 => 60,  173 => 58,  169 => 56,  167 => 55,  163 => 53,  154 => 49,  145 => 47,  142 => 46,  139 => 45,  136 => 44,  133 => 43,  130 => 42,  127 => 41,  124 => 40,  120 => 39,  114 => 36,  111 => 35,  107 => 34,  101 => 31,  98 => 30,  94 => 29,  82 => 19,  78 => 17,  76 => 16,  70 => 12,  62 => 10,  54 => 8,  52 => 7,  44 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/valeo_task/templates/project-listing.html.twig", "/var/www/html/modules/custom/valeo_task/templates/project-listing.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("if" => 7, "for" => 29, "set" => 41);
        static $filters = array("t" => 4, "escape" => 31);
        static $functions = array("path" => 4);

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for', 'set'],
                ['t', 'escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
