<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/valeo_user/templates/user-login.html.twig */
class __TwigTemplate_fe5a9a47c35a329d01adb8244929bc00e112293825033dc0432473f9c17f4dba extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"\">
<div class=\"container\">
    <div class=\"row row-margin-bottom\">
        <div class=\"col-md-8 col-md-offset-2 col-sm-12\">
                <div class=\"formPanel\">
                    <div class=\"row box-shadow\">
                        <div class=\"col-sm-4 col-xs-12 no-padding\">
                            <div class=\"loginImage\">
                                <div class=\"loginText\">
                                </div>
                            </div>
                        </div>
                        <div class=\"col-sm-8 col-xs-12 no-padding\">
                            <div class=\"userForm loginFrom\">
                                <h2>login</h2>
                                <div class=\"formCard\">
                                    <hr>
                                    <article class=\"card-body\">
                                         ";
        // line 19
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["form"] ?? null), 19, $this->source), "html", null, true);
        echo "
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

";
    }

    public function getTemplateName()
    {
        return "modules/custom/valeo_user/templates/user-login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 19,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/valeo_user/templates/user-login.html.twig", "/var/www/html/modules/custom/valeo_user/templates/user-login.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array();
        static $filters = array("escape" => 19);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
