<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/valeo_task/templates/tasks-listing.html.twig */
class __TwigTemplate_1ed7ca83483a311b437f117c3e317405b6370b37d4e0c4d4c9284222fb84f740 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container\">
  <table class=\"table table-sm\">
    <thead>
    <tr >
      <th scope=\"col\">#</th>
      <th scope=\"col\">Type</th>
      <th scope=\"col\">occurrences</th>
      <th scope=\"col\">Result</th>
      <th scope=\"col\">Created At</th>
      <th scope=\"col\">Started At</th>
      <th scope=\"col\">Ended At</th>
    </tr>
    </thead>
    <tbody>
    ";
        // line 15
        if (twig_test_empty(($context["tasks"] ?? null))) {
            // line 16
            echo "      <p> There is no data found</p>
    ";
        } else {
            // line 18
            echo "      ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tasks"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
                // line 19
                echo "        ";
                if ((twig_get_attribute($this->env, $this->source, $context["t"], "task_status", [], "any", false, false, true, 19) == "0")) {
                    // line 20
                    echo "          ";
                    $context["status"] = "fail";
                    // line 21
                    echo "          ";
                    $context["rowClass"] = "table-danger";
                    // line 22
                    echo "          ";
                    $context["occurrences"] = twig_get_attribute($this->env, $this->source, $context["t"], "occurrences", [], "any", false, false, true, 22);
                    // line 23
                    echo "        ";
                } elseif ((twig_get_attribute($this->env, $this->source, $context["t"], "task_status", [], "any", false, false, true, 23) == "1")) {
                    // line 24
                    echo "          ";
                    $context["status"] = "pass";
                    // line 25
                    echo "          ";
                    $context["rowClass"] = "table-success";
                    // line 26
                    echo "          ";
                    $context["occurrences"] = twig_get_attribute($this->env, $this->source, $context["t"], "occurrences", [], "any", false, false, true, 26);
                    // line 27
                    echo "        ";
                } elseif ((twig_get_attribute($this->env, $this->source, $context["t"], "task_status", [], "any", false, false, true, 27) == "2")) {
                    // line 28
                    echo "          ";
                    $context["status"] = "running";
                    // line 29
                    echo "          ";
                    $context["rowClass"] = "";
                    // line 30
                    echo "          ";
                    $context["occurrences"] = ($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["t"], "occurrences", [], "any", false, false, true, 30), 30, $this->source) . "*");
                    // line 31
                    echo "        ";
                }
                // line 32
                echo "
        <tr class=\"";
                // line 33
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["rowClass"] ?? null), 33, $this->source), "html", null, true);
                echo "\">
          <th scope=\"row\">";
                // line 34
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["t"], "task_id", [], "any", false, false, true, 34), 34, $this->source), "html", null, true);
                echo "</th>
          <td> Count ";
                // line 35
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["t"], "task_type", [], "any", false, false, true, 35), 35, $this->source), "html", null, true);
                echo "</td>
          <td>";
                // line 36
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["occurrences"] ?? null), 36, $this->source), "html", null, true);
                echo "</td>
          <td>";
                // line 37
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["status"] ?? null), 37, $this->source), "html", null, true);
                echo "</td>
          <td>";
                // line 38
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_date_format_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["t"], "created", [], "any", false, false, true, 38), 38, $this->source), "Y-m-d H:i"), "html", null, true);
                echo "</td>
          <td>";
                // line 39
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_date_format_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["t"], "start_date", [], "any", false, false, true, 39), 39, $this->source), "Y-m-d H:i"), "html", null, true);
                echo "</td>
          <td>";
                // line 40
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_date_format_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["t"], "end_date", [], "any", false, false, true, 40), 40, $this->source), "Y-m-d H:i"), "html", null, true);
                echo "</td>
        </tr>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "
    ";
        }
        // line 45
        echo "
    </tbody>
  </table>
</div>
";
    }

    public function getTemplateName()
    {
        return "modules/custom/valeo_task/templates/tasks-listing.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 45,  145 => 43,  136 => 40,  132 => 39,  128 => 38,  124 => 37,  120 => 36,  116 => 35,  112 => 34,  108 => 33,  105 => 32,  102 => 31,  99 => 30,  96 => 29,  93 => 28,  90 => 27,  87 => 26,  84 => 25,  81 => 24,  78 => 23,  75 => 22,  72 => 21,  69 => 20,  66 => 19,  61 => 18,  57 => 16,  55 => 15,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/valeo_task/templates/tasks-listing.html.twig", "/var/www/html/modules/custom/valeo_task/templates/tasks-listing.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("if" => 15, "for" => 18, "set" => 20);
        static $filters = array("escape" => 33, "date" => 38);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for', 'set'],
                ['escape', 'date'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
